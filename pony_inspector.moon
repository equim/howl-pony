-- Copyright 2018 The Howl Developers
-- License: MIT (see LICENSE.md at the top-level directory of the distribution)

-- util = bundle_load 'util'
sys = howl.sys
io = howl.io
append = table.insert

(buffer) ->
  path = buffer.file and buffer.file.parent.path or buffer.directory
  ponyc_cmd = sys.find_executable 'ponyc'

  {
    write_stdin: false

    cmd: "#{ponyc_cmd} -V 0 -r parse #{path}"

    is_available: -> ponyc_cmd != nil, "`ponyc` command not found"

    parse: (out) ->
      inspections = {}

      for loc in *io.process_output.parse(out)
        -- Only report errors for the buffer's file.
        if loc.file == buffer.file
          complaint = {
            line: loc.line_nr,
            message: loc.message,
            type: 'error',
          }
          if loc.tokens
            complaint.search = loc.tokens[1]

          append inspections, complaint

      inspections
  }
