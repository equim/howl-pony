-- Copyright 2014-2018 The Howl Developers
-- License: MIT (see LICENSE.md at the top-level directory of the distribution)


-----------------
-- Pony lexer
-----------------
howl.util.lpeg_lexer ->
  c = capture

  combining_ws = c 'combiner', S(' \t\r\n')^1


  -- Identifiers
  ident = (alpha + '_')^1 * (alpha + digit + '_')^0 * P"'"^0
  identifer = c 'identifer', ident


  -- Keywords
  refcaps = c 'keyword', word { 'iso', 'trn', 'ref', 'val', 'box', 'tag' }

  keyword = c 'keyword', word {
    'actor',
    'addressof',
    'and',
    'as',
    'be',
    'box',
    'break',
    'class',
    'compile_error',
    'compile_intrinsic',
    'consume',
    'continue',
    'do',
    'digestof',
    'elseif',
    'else',
    'embed',
    'end',
    'error',
    'false',
    'for',
    'fun',
    'ifdef',
    'iftype',
    'if',
    'interface',
    'in',
    'isnt',
    'iso',
    'is',
    'let',
    'match',
    'new',
    'not',
    'object',
    'or',
    'primitive',
    'recover',
    'ref',
    'repeat',
    'return',
    'struct',
    'tag',
    'then',
    'this',
    'trait',
    'trn',
    'true',
    'try',
    'type',
    'until',
    'use',
    'val',
    'var',
    'where',
    'while',
    'with',
    'xor'
  }


  -- Operators
  operator = c 'operator', S'@=!<>+-/*%^&|~.,:;?()[]{}'


  -- Numbers
  dec_num = digit^1 * ('_' * digit^1)^0
  hex_num = '0' * S'xX' * P'_'^-1 * xdigit^1 * ('_' * xdigit^1)^0
  bin_num = '0' * S'bB' * P'_'^-1 * S'01'^1 * ('_' * S'01'^1)^0
  float = (dec_num * '.' * dec_num * (S'eE' * (P'+' + P'-')^-1 * P'_'^-1 * dec_num)^-1) +
      (dec_num * S'eE' * P'_'^-1 * dec_num)
  chars = span("'", "'", '\\')
  number = c 'number', any {
    float,
    hex_num,
    bin_num,
    dec_num,
    chars
  }


  -- Specials
  special = c 'special', word('_init', '_final', 'true', 'false')


  -- Preprocessor
  preproc = c 'preproc', span('\\', '\\', nil)


  -- Function definitions
  fundef = sequence {
    c('keyword', word { "new", "fun", "be" })
    combining_ws,
    (sequence { preproc, combining_ws } )^-1,
    (sequence { refcaps, combining_ws } )^-1,
    c('fdecl', ident)
  }


  -- Type definitions
  typename = P'_'^-1 * upper^1 * (alpha + digit + '_')^0
  typedef = sequence {
    c('keyword', word {
      'actor', 'class', 'interface', 'trait', 'primitive', 'type', 'struct'
    })
    combining_ws,
    (sequence { preproc, combining_ws } )^-1,
    c('type_def', typename)
  }


  -- Strings
  string = c 'string', any {
    span('"""', '"""', nil),
    span('"', '"', '\\'),
  }


  -- Builtin types
  builtin_type = c 'type', word(
    'I8',
    'I16',
    'I32',
    'I64',
    'ILong',
    'ISize',
    'I128',
    'Signed',
    'None',
    'Env',
    'AsioEventID',
    'AsioEventNotify',
    'AsioEvent',
    'F32',
    'F64',
    'Float',
    'Seq',
    'Real',
    'Integer',
    'FloatingPoint',
    'Number',
    'Int',
    'DoNotOptimise',
    'Bool',
    'AmbientAuth',
    'Iterator',
    'SourceLoc',
    'Array',
    'ArrayKeys',
    'ArrayValues',
    'ArrayPairs',
    'Less',
    'Equal',
    'Greater',
    'Compare',
    'HasEq',
    'Equatable',
    'Comparable',
    'Platform',
    'Stringable',
    'ByteSeq',
    'ByteSeqIter',
    'InputStream',
    'OutStream',
    'StdStream',
    'Any',
    'ReadSeq',
    'ReadElement',
    'U8',
    'U16',
    'U32',
    'U64',
    'ULong',
    'USize',
    'U128',
    'Unsigned',
    'String',
    'StringBytes',
    'StringRunes',
    'InputNotify',
    'DisposableActor',
    'Stdin',
    'Countdown'
  )


  -- Comments
  comment = c 'comment', P'//' * scan_until(eol)
  nested_span = (nested, start_pat, end_pat) ->
    start_pat * ((nested + P 1) - end_pat)^0 * (end_pat + P(-1))
  block_comment = c 'comment', P {
    'block_comment'
    block_comment: nested_span V'block_comment', P'/*', '*/'
  }


  any {
    comment,
    block_comment,
    string,
    operator,
    special,
    fundef,
    typedef,
    keyword,
    number,
    builtin_type,
    preproc,
    identifer
  }
