![Pony](pony.jpg)

Lexer bundle for the Howl editor (http://howl.io/)

(Version 0.2)

# Installation

- Clone or download into `~/.howl/bundles`.

- You may also just copy paste this:

`git clone https://gitlab.com/MarcusE1W/howl-pony ~/.howl/bundles/`


# Contains
- **Pony:** Lexer for a language supporting parallell programming with the actor model in a safe way
